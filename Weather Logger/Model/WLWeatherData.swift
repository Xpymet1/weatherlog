//
//  WLWeatherData.swift
//  Weather Logger
//
//  Created by Admin on 03/10/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class WLWeatherData: WLModelObject
{
    var weaterKind          : String    = ""
    var weaterDescription   : String    = ""
    var weaterIconName      : String    = ""
    var temperature         : Float     = 0
    var temperatureMin      : Float     = 0
    var temperatureMax      : Float     = 0
    var pressure            : Float     = 0
    var humidity            : Float     = 0
    var windSpeed           : Float     = 0
    var visibility          : Float     = 0
    var cloudness           : Int       = 0
    var windDirection       : Float     = 0 // in meteo degrees
    var measureDate         : Double    = 0 // UTC
    
    required convenience init(json: Dictionary<String, AnyObject>)
    {
        self.init()
        
        if  notNullAndNill(object: json["weather"])
        {
            let wArr   = json["weather"] as! [Dictionary<String, AnyObject>]
            
            if  wArr.count > 0
            {
                let wDic    = wArr.first!
                 
                if  notNullAndNill(object: wDic["main"])
                {
                    let wKind   = wDic["main"] as! String
                    
                    weaterKind  = wKind
                }
                
                if  notNullAndNill(object: wDic["description"])
                {
                    let wDescription    = wDic["description"] as! String
                    
                    weaterDescription   = wDescription
                }
                
                if  notNullAndNill(object: wDic["icon"])
                {
                    let wIconName   = wDic["icon"] as! String
                    
                    weaterIconName  = wIconName
                }
            }
        }
        
        if  notNullAndNill(object: json["main"])
        {
            let wMain   = json["main"] as! Dictionary<String, AnyObject>

            if  notNullAndNill(object: wMain["temp"])
            {
                let wTemperature    = wMain["temp"] as! NSNumber
                
                temperature         = wTemperature.floatValue
            }
            
            if  notNullAndNill(object: wMain["temp_min"])
            {
                let wTemperatureMin = wMain["temp_min"] as! NSNumber
                
                temperatureMin      = wTemperatureMin.floatValue
            }
            
            if  notNullAndNill(object: wMain["temp_max"])
            {
                let wTemperatureMax = wMain["temp_max"] as! NSNumber
                
                temperatureMax      = wTemperatureMax.floatValue
            }
            
            if  notNullAndNill(object: wMain["pressure"])
            {
                let wPressure   = wMain["pressure"] as! NSNumber
                
                pressure        = wPressure.floatValue
            }
            
            if  notNullAndNill(object: wMain["humidity"])
            {
                let wHumidity   = wMain["humidity"] as! NSNumber
                
                humidity        = wHumidity.floatValue
            }
        }
         
        if  notNullAndNill(object: json["wind"])
        {
            let wWind   = json["wind"] as! Dictionary<String, AnyObject>
             
            if  notNullAndNill(object: wWind["speed"])
            {
                let wSpeed  = wWind["speed"] as! NSNumber
                
                windSpeed   = wSpeed.floatValue
            }
            
            if  notNullAndNill(object: wWind["deg"])
            {
                let wDirection  = wWind["deg"] as! NSNumber
                
                windDirection   = wDirection.floatValue
            }
        }
        
        if  notNullAndNill(object: json["dt"])
        {
            let wMeasureDate    = json["dt"] as! NSNumber
            
            measureDate         = wMeasureDate.doubleValue
        }
        
        if  notNullAndNill(object: json["visibility"])
        {
            let wVisibility = json["visibility"] as! Float
            
            visibility      = wVisibility
        }
        
        if  notNullAndNill(object: json["clouds"])
        {
            let wCloudsJson = json["clouds"] as! Dictionary<String, AnyObject>
            if  let wClouds = wCloudsJson["all"] as? Int
            {
                cloudness   = wClouds
            }
        }
    }
    
    func readableTempString() -> String
    {
        return "\( Int(temperature) )°C"
    }
    
    func readableDateString() -> String
    {
        let date    = Date(timeIntervalSince1970: measureDate )
        
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "HH:mm dd.MM.yyyy"
        dateFormatter.timeZone      = TimeZone.current
        
        let result  = dateFormatter.string(from: date)

        return result
    }

    func readableFullDateString() -> String
    {
        let date    = Date(timeIntervalSince1970: measureDate )
        
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "HH:mm d MMMM yyyy"
        dateFormatter.timeZone      = TimeZone.current
        
        let result  = dateFormatter.string(from: date)

        return result
    } 

    func windDirectionCode() -> String
    {
        switch windDirection
        {
            case 0...11.25:
                return "N"
            
            case 11.25...33.75:
                return "NNE"
            
            case 33.75...56.25:
                return "NE"
            
            case 56.25...78.75:
                return "ENE"
            
            case 78.75...101.25:
                return "E"
            
            case 101.25...123.75:
                return "ESE"
            
            case 123.75...146.25:
                return "SE"
            
            case 146.25...168.75:
                return "SSE"
            
            case 168.75...191.25:
                return "S"
            
            case 191.25...213.75:
                return "SSW"
            
            case 213.75...236.25:
                return "SW"
            
            case 236.25...258.75:
                return "WSW"
            
            case 258.75...281.25:
                return "W"
            
            case 281.25...303.75:
                return "WNW"
            
            case 303.75...326.25:
                return "NW"
            
            case 326.25...348.75:
                return "NNW"
            
            case 348.75...360:
                return "N"
            
            default: return ""
        }
    }
}
