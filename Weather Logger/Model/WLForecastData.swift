//
//  WLForecastData.swift
//  Weather Logger
//
//  Created by Admin on 04/11/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class WLForecastData: WLModelObject
{
    var forecastLocation    : WLWeatherLocation?
    var forecastList        : [WLWeatherData]   = []
    
    required convenience init(json: Dictionary<String, AnyObject>)
    {
        self.init()
        
        if  notNullAndNill(object: json["city"])
        {
            let fLoc    = json["city"] as! Dictionary<String, AnyObject>
            
            forecastLocation    = WLWeatherLocation(json: fLoc )
        }
        
        if  notNullAndNill(object: json["list"])
        {
            let fList   = json["list"] as! [Dictionary<String, AnyObject>]
            
            var finalList   = [WLWeatherData]()
            
            for item in fList
            {
                let wData   = WLWeatherData(json: item)
                
                finalList.append(wData)
            }
            
            forecastList    = finalList 
        }
    }
    
    func weatherDataIn(days: Int) -> WLWeatherData?
    {
        let dataIndex   = days * 8
        if  dataIndex > forecastList.count - 1 { return nil }
         
        return forecastList[dataIndex]
    }
    
    func weatherDataArrFor(day: Int) -> [WLWeatherData]
    {
        var newArr      = [WLWeatherData]()
        let rangeStart  = day * 8
        let arrRange    = rangeStart..<rangeStart + 8
        
        newArr.append(contentsOf: forecastList[arrRange] )
        
        return newArr
    }
}
