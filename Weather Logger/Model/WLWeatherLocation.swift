//
//  WLWeatherLocation.swift
//  Weather Logger
//
//  Created by Admin on 07/11/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class WLWeatherLocation: WLModelObject
{
    static let locationsList    = prepareLocationsList()
    
    static let storedLocationKey    = "storedLocationKey"
    
    var countryCode     : String    = ""
    var locationID      : Int       = -1
    var locationName    : String    = ""
    
    required convenience init(json: Dictionary<String, AnyObject>)
    {
        self.init()
        
        if  notNullAndNill(object: json["country"])
        {
            let lCode   = json["country"] as! String
            
            countryCode = lCode
        }
        
        if  notNullAndNill(object: json["id"])
        {
            let lId     = json["id"] as! Int
            
            locationID  = lId
        }
        
        if  notNullAndNill(object: json["name"])
        {
            let lName       = json["name"] as! String
            
            locationName    = lName
        }
    }
    
    func isFavorite() -> Bool
    {
        let lArr    = WLWeatherLocation.favoriteLocationsList()
        
        return lArr.contains(self.locationID)
    }
    
    class func prepareLocationsList() -> [WLWeatherLocation]
    {
        var locations   = [WLWeatherLocation]()
        
        if  let path = Bundle.main.path(forResource: "locationsList", ofType: "json")
        {
            do
            {
                let data        = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult  = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                if  let jsonResult = jsonResult as? [Dictionary<String, AnyObject>]
                {
                    for locJson in jsonResult
                    {
                        let location = WLWeatherLocation(json: locJson)
                        locations.append(location)
                    }
                }
            }
            catch
            {
                print("failed parsing locList")
            }
        }
        
        return locations
    }
    
//    MARK: - Stored Locations
     
    class func favoriteLocationsList() -> [Int]
    {
        let defaults        = UserDefaults.standard
        let locationData    = defaults.data(forKey: WLWeatherLocation.storedLocationKey)
        
        if  locationData == nil { return [] }
        
        if  let locationArr = NSKeyedUnarchiver.unarchiveObject(with: locationData! ) as? [Int]
        {
            return locationArr
        }

        return []
    }
    
    func addLocationToStorage()
    {
        var lArr    = WLWeatherLocation.favoriteLocationsList()
        
        lArr.append( self.locationID )
        
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: lArr)
        let defaults    = UserDefaults.standard
        
        defaults.set(encodedData, forKey: WLWeatherLocation.storedLocationKey)
        defaults.synchronize()
    }
    
    func removeLocationFromStorage()
    {
        var lArr    = WLWeatherLocation.favoriteLocationsList()
        
        if  let index = lArr.firstIndex(where: { (sData) -> Bool in return sData == self.locationID })
        {
            lArr.remove(at: index )
        }
        else { return }
        
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: lArr)
        let defaults    = UserDefaults.standard
        
        defaults.set(encodedData, forKey: WLWeatherLocation.storedLocationKey )
        defaults.synchronize()
    }
}
