//
//  WLModelObject.swift
//  Weather Logger
//
//  Created by Admin on 03/10/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class WLModelObject: NSObject
{
    override init()
    {
        
    }
    
    required convenience init(json: Dictionary<String, AnyObject>)
    {
        self.init()
        
        
    }
    
    required convenience init(json: [Dictionary<String, AnyObject>])
    {
        self.init()
        
        
    }
    
    func notNullAndNill(object: Any?) -> Bool
    {
        if (object == nil || object is NSNull)
        {
            return false
        }
        return true
    }
    
}
