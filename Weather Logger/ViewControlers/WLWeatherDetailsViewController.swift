//
//  WLWeatherDetailsViewController.swift
//  Weather Logger
//
//  Created by Admin on 05/10/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import Charts

class WLWeatherDetailsViewController: UIViewController, ChartViewDelegate, IAxisValueFormatter
{
    @IBOutlet weak var backgroundImage          : UIImageView!
    @IBOutlet weak var chartView                : LineChartView!
    @IBOutlet weak var mainWeatherDetailsView   : WLWeatherMainDetailsView!
    
    var weatherForecast : WLForecastData!
    var weatherDetails  : WLWeatherData!
    var graphLabels     = [String]()
    var selectedDay     = 0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        weatherDetails  = weatherForecast.weatherDataIn(days: selectedDay )
         
        prepareView()
        setupLineChartView()
    }
    
    func prepareView()
    {
        mainWeatherDetailsView.prepareView(with: weatherDetails, for: weatherForecast.forecastLocation )
        
        backgroundImage.image   = UIImage(named: "02_cloud") // default
        
        if      weatherDetails.weaterIconName.contains("01")
        {
            backgroundImage.image   = UIImage(named: "01_sun")
        }
        else if weatherDetails.weaterIconName.contains("02")
        {
            backgroundImage.image   = UIImage(named: "02_cloud")
        }
        else if weatherDetails.weaterIconName.contains("09")
        {
            backgroundImage.image   = UIImage(named: "09_rain")
        }
        else if weatherDetails.weaterIconName.contains("10")
        {
            backgroundImage.image   = UIImage(named: "09_rain")
        }
    }
    
    func setupLineChartView()
    {
        chartView.setViewPortOffsets(left: 20, top: 20, right: 20, bottom: 20)
        chartView.backgroundColor           = UIColor.white.withAlphaComponent(0.5)
        chartView.delegate                  = self
        chartView.legend.enabled            = false
        chartView.chartDescription?.enabled = false
        chartView.isUserInteractionEnabled  = false
        chartView.pinchZoomEnabled          = false
        chartView.rightAxis.enabled         = false
        chartView.minOffset                 = 0
        chartView.drawBordersEnabled        = false
        chartView.setScaleEnabled(false)
        
        chartView.xAxis.enabled                         = true
        chartView.xAxis.avoidFirstLastClippingEnabled   = true
        chartView.xAxis.drawAxisLineEnabled             = false
        chartView.xAxis.drawGridLinesEnabled            = false
        chartView.xAxis.labelPosition                   = XAxis.LabelPosition.bottom
        chartView.xAxis.valueFormatter                  = self
        
        chartView.leftAxis.axisLineColor        = .clear
        chartView.leftAxis.enabled              = false
        chartView.leftAxis.labelPosition        = YAxis.LabelPosition.insideChart
        chartView.leftAxis.drawGridLinesEnabled = false
        chartView.leftAxis.gridColor            = UIColor.clear
        
        let gradientColors  = [ UIColor.white.withAlphaComponent(0.25).cgColor, UIColor.white.cgColor ]
        let gradient        = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        var dataEntries : [ChartDataEntry]  = []
        let dataArr     = weatherForecast.weatherDataArrFor(day: selectedDay)
         
        for index in 0..<dataArr.count
        {
            let wData   = dataArr[index]
            let hourStr = Date(timeIntervalSince1970: wData.measureDate ).hourOfDayString()
            graphLabels.append(hourStr)
             
            let dataEntry   = ChartDataEntry(x: Double( index ), y: Double( wData.temperature ) )
            dataEntries.append(dataEntry)
        }
        
        let min = dataEntries.min { (first, second) -> Bool in return first.y < second.y }
        let max = dataEntries.max { (first, second) -> Bool in return first.y < second.y }
        
        chartView.leftAxis.axisMaximum  = Double( max?.y ?? 0) + 5.0
        chartView.leftAxis.axisMinimum  = Double( min?.y ?? 0) - 5.0
        
        let lineChartDataSet                    = LineChartDataSet(entries: dataEntries, label: "Hour")
        lineChartDataSet.mode                   = .horizontalBezier
        lineChartDataSet.lineWidth              = 0.7
        lineChartDataSet.highlightColor         = UIColor.red
        lineChartDataSet.drawValuesEnabled      = false
        lineChartDataSet.drawFilledEnabled      = true
        lineChartDataSet.fillAlpha              = 1
        lineChartDataSet.fill                   = Fill(linearGradient: gradient, angle: 90)
        lineChartDataSet.circleRadius           = 2
        lineChartDataSet.circleHoleRadius       = 1
        lineChartDataSet.drawCirclesEnabled     = true
        lineChartDataSet.drawCircleHoleEnabled  = true
        lineChartDataSet.setColor( UIColor.blue )
        lineChartDataSet.setCircleColor( UIColor.white )
        lineChartDataSet.drawValuesEnabled      = true
        
        var dataSets    = [IChartDataSet]()
        dataSets.append(lineChartDataSet)
        
        let lineChartData   = LineChartData(dataSets: dataSets)
        chartView.data      = lineChartData
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String
    {
        return graphLabels[Int(value)]
    }
}
