//
//  WLModelViewController.swift
//  Weather Logger
//
//  Created by Admin on 06/10/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class WLModelViewController: UIViewController
{
    var activityView    : UIView?
    
    func enableActivityView()
    {
        navigationController?.navigationBar.isUserInteractionEnabled    = false
        view.isUserInteractionEnabled                                   = false
        createActivityView()
    }
    
    func disableActivityView()
    {
        navigationController?.navigationBar.isUserInteractionEnabled    = true
        view.isUserInteractionEnabled                                   = true
        activityView?.removeFromSuperview()
        activityView    = nil
    }
    
    private func createActivityView()
    {
        if  activityView == nil
        {
            let holderView      = UIView()
            let holderSide      = view.frame.width * 0.33
            
            holderView.frame    = CGRect(x: 0, y: 0, width: holderSide, height: holderSide)
            holderView.center   = CGPoint(x: view.frame.width * 0.5, y: view.frame.height * 0.5)
            
            holderView.clipsToBounds        = true
            holderView.layer.cornerRadius   = 8
            holderView.backgroundColor      = UIColor.WL_LightBlack1Color().withAlphaComponent(0.7)
            
            let activityIndicator       = UIActivityIndicatorView(style: .whiteLarge)
            activityIndicator.center    = CGPoint(x: holderView.frame.width * 0.5, y: holderView.frame.height * 0.5)
            
            holderView.addSubview(activityIndicator)
            view.addSubview(holderView)
            
            activityIndicator.startAnimating()
            activityView    = holderView
        }
    }
}
