//
//  WLLocationsListViewController.swift
//  Weather Logger
//
//  Created by Admin on 08/11/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

protocol LocationSelectedDelegate
{
    func locationSelected(_ loc: WLWeatherLocation)
}

class WLLocationsListViewController: WLModelViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate
{
    @IBOutlet weak var locationTableView    : UITableView!
    @IBOutlet weak var searchBar            : UISearchBar!
    
    var locationDelegate   : LocationSelectedDelegate?
    
    var searchTimer     : Timer?
    var locationsList   = WLWeatherLocation.locationsList
    
    var favoriteLocationsArr    = [WLWeatherLocation]()
    var searchLocationsArr      = [WLWeatherLocation]()
    
    var searchInProgress    = false
     
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        prepareFavoriteList()
    }
    
    func prepareFavoriteList()
    {
        let favoriteList    = WLWeatherLocation.favoriteLocationsList()
        let filtered        = locationsList.filter
        { (location) -> Bool in
            return favoriteList.contains(location.locationID)
        }
        
        favoriteLocationsArr    = filtered
    }
    
    func filterBySearchText(_ text: String)
    {
        searchLocationsArr  = locationsList.filter
        { (location) -> Bool in
            return location.locationName.contains(text)
        }
        
        DispatchQueue.main.async
        {
            self.disableActivityView()
            self.locationTableView.reloadData()
            print("found \(self.searchLocationsArr.count) results for searchText = \(text)")
        }
    }
    
    func hideKeyboard()
    {
        view.endEditing(true)
    }
    
    @IBAction func toggleFav(sender: UIButton)
    {
        let locIndex    = sender.tag
        
        var loc : WLWeatherLocation!
       
        if  searchInProgress
        {
            loc = searchLocationsArr[locIndex]
        }
        else
        {
            loc = favoriteLocationsArr[locIndex]
        }
        
        if  loc.isFavorite()
        {
            loc.removeLocationFromStorage()
        }
        else
        {
            loc.addLocationToStorage()
        }
        
        locationTableView.reloadRows(at: [IndexPath(item: locIndex, section: 0)], with: .automatic)
    }
    
// MARK: - Search Field Delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        searchTimer?.invalidate()
        
        print("search text - \(searchText)")
        
        if searchText.count < 1
        {
            tryToSearch()
            
            return
        }
        
        self.searchTimer = Timer.scheduledTimer(timeInterval: 0.75, target: self, selector: #selector(self.tryToSearch), userInfo: nil, repeats: false)
    }
    
    @objc func tryToSearch()
    {
        let searchText  = searchBar.text ?? ""
        
        if  searchText.count < 2
        {
            searchInProgress = false
            locationTableView.reloadData()
        }
        else
        {
            searchInProgress    = true
            
            self.enableActivityView()
            
            DispatchQueue.global().async
            {
                self.filterBySearchText(searchText)
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        tryToSearch()
        hideKeyboard()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.text  = ""
        tryToSearch()
        hideKeyboard()
    }
    
    // MARK: - Table Methods
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if searchInProgress { return searchLocationsArr.count }
        
        return favoriteLocationsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell    = tableView.dequeueReusableCell(withIdentifier: WLLocationCell.reuseId, for: indexPath) as! WLLocationCell
         
        var loc : WLWeatherLocation!
        
        if  searchInProgress
        {
            loc = searchLocationsArr[indexPath.row]
        }
        else
        {
            loc = favoriteLocationsArr[indexPath.row]
        }
        
        cell.prepareCell(with: loc)
        cell.favoriteButton.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var loc : WLWeatherLocation!
       
        if  searchInProgress
        {
            loc = searchLocationsArr[indexPath.row]
        }
        else
        {
            loc = favoriteLocationsArr[indexPath.row]
        }
        
        locationDelegate?.locationSelected(loc)
        dismiss(animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        hideKeyboard()
    }
}
