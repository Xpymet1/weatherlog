//
//  WLWeatherMainViewController.swift
//  Weather Logger
//
//  Created by Admin on 07/11/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

import UIKit

class WLWeatherMainViewController : WLModelViewController, LocationDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, LocationSelectedDelegate
{
    @IBOutlet weak var mainWeatherDetailsView   : WLWeatherMainDetailsView!
    @IBOutlet weak var weatherCollectionView    : UICollectionView!
    
    @IBOutlet weak var backgroundImageView      : UIImageView!
    
    var weatherForecast : WLForecastData?
    var grabbingData    = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        WLLocationManager.shared.locationDelegate   = self
         
        prepareView()
        applyParallax()
        
        grabDataAction()
    }
    
    func prepareView()
    {
        backgroundImageView.image   = UIImage(named: "02_cloud")
        
        self.navigationController?.navigationBar.isTranslucent          = true
        self.navigationController?.navigationBar.titleTextAttributes    = [.foregroundColor: UIColor.white ]
        self.navigationController?.navigationBar.tintColor              = .white
        self.navigationController?.navigationBar.barTintColor           = .WL_LightBlack1Color()
 
        self.title = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String ?? ""
        
        let oneTap = UITapGestureRecognizer(target: self, action: #selector(self.mainDetailsTapAction) )
        mainWeatherDetailsView.addGestureRecognizer(oneTap)
    }
    
    func applyParallax()
    {
        let min = CGFloat(-30)
        let max = CGFloat(30)
              
        let xMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.x", type: .tiltAlongHorizontalAxis)
        xMotion.minimumRelativeValue    = min
        xMotion.maximumRelativeValue    = max
              
        let yMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.y", type: .tiltAlongVerticalAxis)
        yMotion.minimumRelativeValue    = min
        yMotion.maximumRelativeValue    = max
              
        let motionEffectGroup   = UIMotionEffectGroup()
        motionEffectGroup.motionEffects = [xMotion, yMotion]

        backgroundImageView.addMotionEffect(motionEffectGroup)
    }
    
    func grabDataForLocation(_ loc: WLWeatherLocation)
    {
        enableActivityView()
        
        WLWeatherGrabber.shared.grabForecastForCity( loc.locationID )
        { (success, errorStr, data) -> Void in
             
            self.disableActivityView()
            
            if  success
            {
                if  let wData   = data
                {
                    self.weatherForecast    = wData
                    self.mainWeatherDetailsView.isHidden    = false
                    
                    if  let fData   = wData.weatherDataIn(days: 0 )
                    {
                        self.mainWeatherDetailsView.prepareView(with: fData, for: wData.forecastLocation)
                    }
                    
                    self.weatherCollectionView.reloadData()
                }
            }
            else
            {
                let alertController = UIAlertController (title: "Something went wrong", message: errorStr, preferredStyle: .alert)
                
                let okAction    = UIAlertAction(title: "Ok", style: .default)
                alertController.addAction(okAction)

                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - Actions
    
    func grabDataAction()
    {
        print("grab action")
        
        grabbingData    = true
        
        let status  = WLLocationManager.shared.isServiceEnabled()
        
        if      status == .notDetermined
        {
            WLLocationManager.shared.askForPermission()
        }
        else if status == .denied || status == .restricted
        {
            let alertController = UIAlertController (title: "Location Services", message: "To provide weather data we must know your location, please go to your iPhone Settings and authorize use of location services", preferredStyle: .alert)

            let settingsAction  = UIAlertAction(title: "Settings", style: .default)
            { (_) -> Void in

                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }

                if UIApplication.shared.canOpenURL(settingsUrl)
                {
                    UIApplication.shared.open(settingsUrl, completionHandler:
                    { (success) in
                        print("Settings opened: \(success)")
                    })
                }
            }
            
            alertController.addAction(settingsAction)

            let cancelAction    = UIAlertAction(title: "Cancel", style: .default)
            { (_) -> Void in
                 //do nothing right now
            }
             
            alertController.addAction(cancelAction)

            present(alertController, animated: true, completion: nil)
        }
        else
        {
            enableActivityView()
            WLLocationManager.shared.findLocation()
        }
    }
    
    @objc func mainDetailsTapAction()
    {
        openDetailsForDay(0)
    }
    
    func openDetailsForDay(_ day: Int)
    {
        performSegue(withIdentifier: "toDetails", sender: day )
    }
    
    @IBAction func openSearchScreen()
    {
        performSegue(withIdentifier: "toSearch", sender: nil )
    }
    
//    MARK: - Location Selected Delegate
    
    func locationSelected(_ loc: WLWeatherLocation)
    {
        grabDataForLocation(loc)
    }
    
//    MARK: - Location Delegate
     
    func locationFound(_ lat: Double, _ lon: Double)
    {
        if  grabbingData == true
        {
            grabbingData    = false
            
            WLWeatherGrabber.shared.grabForecastForLocation(lat, lon,
            { (success, errorStr, data) -> Void in
                
                self.disableActivityView()
                
                if  success
                {
                    if  let wData   = data
                    {
                        self.weatherForecast    = wData
                        self.mainWeatherDetailsView.isHidden    = false
                        
                        if  let fData   = wData.weatherDataIn(days: 0 )
                        {
                            self.mainWeatherDetailsView.prepareView(with: fData, for: wData.forecastLocation)
                        }
                        
                        self.weatherCollectionView.reloadData()
                    }
                }
                else
                {
                    let alertController = UIAlertController (title: "Something went wrong", message: errorStr, preferredStyle: .alert)
                    
                    let okAction    = UIAlertAction(title: "Ok", style: .default)
                    alertController.addAction(okAction)

                    self.present(alertController, animated: true, completion: nil)
                }
            })
        }
            
    }
    
//    MARK: - Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if      segue.identifier == "toDetails"
        {
            let destV   = segue.destination as! WLWeatherDetailsViewController
            
            destV.weatherForecast   = weatherForecast
            destV.selectedDay       = sender as! Int
        }
        else if  segue.identifier == "toSearch"
        {
            let destV   = segue.destination as! WLLocationsListViewController
            
            destV.locationDelegate  = self
        }
    }
    
//  MARK: - Collection Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if  weatherForecast == nil { return 0 }
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellWidth   = UIScreen.main.bounds.width * 0.25
        let cellHeight  = collectionView.frame.height
 
        return CGSize(width: cellWidth, height: cellHeight )
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell    = collectionView.dequeueReusableCell(withReuseIdentifier: WLWeatherDayCell.reuseId, for: indexPath) as! WLWeatherDayCell
        
        let day     = indexPath.row + 1
        
        if  let wData   = weatherForecast?.weatherDataIn(days: day)
        {
            cell.prepareCell(with: wData)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("CollCell selected - \(indexPath)")
        
        openDetailsForDay(indexPath.row + 1)
    }
}
