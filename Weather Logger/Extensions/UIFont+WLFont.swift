//
//  UIFont+WLFont.swift
//  Weather Logger
//
//  Created by Admin on 05/10/2020.
//  Copyright © 2020 Admin. All rights reserved.
//
 
import UIKit

extension UIFont
{
    static let spaceGroteskRegularFontName  = "SpaceGrotesk-Regular"
    static let spaceGroteskBoldFontName     = "SpaceGrotesk-Bold"
    static let spaceGroteskSemiboldFontName = "SpaceGrotesk-SemiBold"
    static let spaceGroteskLightFontName    = "SpaceGrotesk-Light"
    
    class func WL_SpaceGroteskRegular(withSize size: CGFloat) -> UIFont
    {
        return UIFont(name: spaceGroteskRegularFontName, size: size)!
    }
    
    class func WL_SpaceGroteskBold(withSize size: CGFloat) -> UIFont
    {
        return UIFont(name: spaceGroteskBoldFontName, size: size)!
    }
    
    class func WL_SpaceGroteskSemibold(withSize size: CGFloat) -> UIFont
    {
        return UIFont(name: spaceGroteskSemiboldFontName, size: size)!
    }
    
    class func WL_SpaceGroteskLight(withSize size: CGFloat) -> UIFont
    {
        return UIFont(name: spaceGroteskLightFontName, size: size)!
    }
}
