//
//  Date+WLDate.swift
//  Weather Logger
//
//  Created by Admin on 08/11/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

extension Date
{
    func dayString() -> String
    {
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "EEEE"
        dateFormatter.timeZone      = TimeZone.current
        
        let result  = dateFormatter.string(from: self)

        return result
    }
    
    func hourOfDayString() -> String
    {
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "ha"
        dateFormatter.timeZone      = TimeZone.current
        
        let result  = dateFormatter.string(from: self)

        return result
    }
}
