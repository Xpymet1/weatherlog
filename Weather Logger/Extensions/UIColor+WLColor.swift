//
//  UIColor+WLColor.swift
//  Weather Logger
//
//  Created by Admin on 05/10/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

extension UIColor
{
    class func WL_LightBlack1Color () -> UIColor
    {
        return UIColor.init(red: 72.0 / 255.0, green: 72.0 / 255.0, blue: 74.0 / 255.0, alpha: 1)
    }
    
    class func WL_LightBlue1Color () -> UIColor
    {
        return UIColor.init(red: 0 / 255.0, green: 151.0 / 255.0, blue: 230.0 / 255.0, alpha: 1)
    }
    
    class func WL_Orange1Color () -> UIColor
    {
        return UIColor.init(red: 235 / 255.0, green: 110.0 / 255.0, blue: 75.0 / 255.0, alpha: 1)
    }
    
    class func WL_LightBlue2Color () -> UIColor
    {
        return UIColor.init(red: 141 / 255.0, green: 201.0 / 255.0, blue: 191.0 / 255.0, alpha: 1)
    }
}
