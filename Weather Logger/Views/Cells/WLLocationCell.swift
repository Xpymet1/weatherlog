//
//  WLLocationCell.swift
//  Weather Logger
//
//  Created by Admin on 08/11/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class WLLocationCell: UITableViewCell
{
    static let reuseId  = "locationCell"
   
    @IBOutlet weak var locationNameLabel : UILabel!
    @IBOutlet weak var favoriteImageView : UIImageView!
    @IBOutlet weak var favoriteButton    : UIButton!
   
    func prepareCell(with wLoc: WLWeatherLocation )
    {
        locationNameLabel.font      = .WL_SpaceGroteskLight(withSize: 15.0 )
        locationNameLabel.textColor = .black
 
        locationNameLabel.text  = wLoc.locationName
        
        favoriteImageView.image = UIImage(named: "favorit_off")
        
        if wLoc.isFavorite() { favoriteImageView.image = UIImage(named: "favorit_on") }
    }
}
