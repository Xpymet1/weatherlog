//
//  WLWeatherDayCell.swift
//  Weather Logger
//
//  Created by Admin on 07/11/2020.
//  Copyright © 2020 Admin. All rights reserved.
//
 
import UIKit

class WLWeatherDayCell: UICollectionViewCell
{
    static let reuseId  = "weatherDayCell"
    
    @IBOutlet weak var dateLabel        : UILabel!
    @IBOutlet weak var iconImageView    : UIImageView!
    @IBOutlet weak var tempLabel        : UILabel!
    
    func prepareCell(with wData: WLWeatherData )
    {
        dateLabel.font      = .WL_SpaceGroteskLight(withSize: 15.0 )
        tempLabel.font      = .WL_SpaceGroteskLight(withSize: 15.0 )
        iconImageView.image = UIImage(named: wData.weaterIconName )
        dateLabel.textColor = .white
        tempLabel.textColor = .white

        let dataDate    = Date(timeIntervalSince1970: wData.measureDate )
        
        dateLabel.text  = dataDate.dayString().capitalized
        tempLabel.text  = wData.readableTempString()
    }
}
