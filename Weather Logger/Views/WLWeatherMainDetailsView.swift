//
//  WLWeatherMainDetailsView.swift
//  Weather Logger
//
//  Created by Admin on 07/11/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class WLWeatherMainDetailsView: UIView
{
    @IBOutlet weak var cityNameLabel            : UILabel!
    @IBOutlet weak var weatherKindLabel         : UILabel!
    @IBOutlet weak var weatherTempLabel         : UILabel!
    @IBOutlet weak var weatherTempMinLabel      : UILabel!
    @IBOutlet weak var weatherTempMaxLabel      : UILabel!
    @IBOutlet weak var weatherMeasureDateLabel  : UILabel!
    @IBOutlet weak var weatherPressureLabel     : UILabel!
    @IBOutlet weak var weatherHumidityLabel     : UILabel!
    @IBOutlet weak var windSpeedLabel           : UILabel!
    @IBOutlet weak var windDirectionImageView   : UIImageView!
    @IBOutlet weak var windDirectionLabel       : UILabel!
    
    @IBOutlet weak var detailsHolderView        : UIView!
   
    func prepareView(with data: WLWeatherData, for location: WLWeatherLocation?)
    {
        cityNameLabel.text              = location?.locationName ?? ""
        weatherKindLabel.text           = data.weaterDescription.capitalized
        weatherTempLabel.text           = data.readableTempString()
        weatherTempMinLabel.text        = "Min: \(data.temperatureMin)"
        weatherTempMaxLabel.text        = "Max: \(data.temperatureMax)"
        weatherMeasureDateLabel.text    = data.readableFullDateString()
        windDirectionLabel.text         = "Wind direction: " + data.windDirectionCode()
        windSpeedLabel.text             = "\( data.windSpeed )m/s"
        weatherHumidityLabel.text       = "\( Int(data.humidity) )%"
        weatherPressureLabel.text       = "\( Int(data.pressure) )hPa"
          
        
        cityNameLabel.font              = .WL_SpaceGroteskBold(withSize: 24.0 )
        weatherKindLabel.font           = .WL_SpaceGroteskSemibold(withSize: 18.0 )
        
        weatherTempLabel.font           = .WL_SpaceGroteskRegular(withSize: 50.0 )
        weatherTempMinLabel.font        = .WL_SpaceGroteskLight(withSize: 13.0 )
        weatherTempMaxLabel.font        = .WL_SpaceGroteskLight(withSize: 13.0 )
        
        weatherMeasureDateLabel.font    = .WL_SpaceGroteskRegular(withSize: 18.0 )
        windDirectionLabel.font         = .WL_SpaceGroteskLight(withSize: 13.0 )
        windSpeedLabel.font             = .WL_SpaceGroteskLight(withSize: 13.0 )
        weatherHumidityLabel.font       = .WL_SpaceGroteskLight(withSize: 13.0 )
        weatherPressureLabel.font       = .WL_SpaceGroteskLight(withSize: 13.0 )
        
        
        cityNameLabel.textColor         = .white
        weatherKindLabel.textColor      = .white
        
        weatherTempLabel.textColor      = .white
        weatherTempMinLabel.textColor   = .white
        weatherTempMaxLabel.textColor   = .white
        
        weatherMeasureDateLabel.textColor   = .white
        windDirectionLabel.textColor        = .white
        windSpeedLabel.textColor            = .white
        weatherHumidityLabel.textColor      = .white
        weatherPressureLabel.textColor      = .white
        
        detailsHolderView.clipsToBounds         = true
        detailsHolderView.layer.cornerRadius    = 8
        detailsHolderView.backgroundColor       = UIColor.WL_LightBlue2Color().withAlphaComponent(0.8)
    }
}
