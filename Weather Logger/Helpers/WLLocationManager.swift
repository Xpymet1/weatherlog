//
//  WLLocationManager.swift
//  Weather Logger
//
//  Created by Admin on 03/10/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import CoreLocation
import Foundation

protocol LocationDelegate
{
    func locationFound(_ lat: Double, _ lon: Double)
}

class WLLocationManager: NSObject, CLLocationManagerDelegate
{
    static  let shared  = WLLocationManager()
    
    private let locationManager = CLLocationManager()
    
    var locationDelegate    : LocationDelegate?
    
    override init()
    {
        super.init()
        
        locationManager.delegate    = self
    }

    func isServiceEnabled() -> CLAuthorizationStatus
    {
        if CLLocationManager.locationServicesEnabled()
        {
            let status  = CLLocationManager.authorizationStatus()
            
            if  status == .authorizedAlways || status == .authorizedWhenInUse
            {
                locationManager.startUpdatingLocation()
            }
            
            return status
        }
        
        return .notDetermined
    }
    
    func askForPermission()
    {
        locationManager.requestAlwaysAuthorization()
    }
    
    func findLocation()
    {
        locationManager.startUpdatingLocation()
    }
    
    // MARK: - Location Delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let location : CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        print("Got location")

        locationManager.stopUpdatingLocation()
        
        let latitude    = location.latitude
        let longitude   = location.longitude
        
        locationDelegate?.locationFound(latitude, longitude)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        print("status change")
        
        if  status == .authorizedAlways || status == .authorizedWhenInUse
        {
            WLLocationManager.shared.locationManager.startUpdatingLocation()
        }
    }
}
