//
//  WLWeatherGrabber.swift
//  Weather Logger
//
//  Created by Admin on 03/10/2020.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import AFNetworking

class WLWeatherGrabber
{
    static  let shared  = WLWeatherGrabber() 
     
    private let baseForecastURL = "http://api.openweathermap.org/data/2.5/forecast?"
    private let apiKey          = "6e9306dd9df582316ed2e3620c221536"
    
    func grabForecastForLocation(_ lat: Double, _ lon: Double, _ completionHandler: @escaping ((_ success: Bool, _ errorStr: String?, _ data: WLForecastData?) -> Void))
    {
        let paramsStr   = "lat=\(lat)&lon=\(lon)&appid=\(apiKey)&units=metric"
        let manager     = AFHTTPSessionManager()
        
        guard let requestUrl    = URL(string: baseForecastURL + paramsStr ) else
        {
            completionHandler(false, "requestUrl failed", nil)
            return
        }
        
        let urlRequest  = URLRequest(url: requestUrl )
        print("\(requestUrl.absoluteString)")
        
        let task    = manager.dataTask(with: urlRequest  , uploadProgress: nil, downloadProgress: nil)
        { (response, data, error) in
            
            if  error == nil
            {
                print("Weather Grab Successful")
                
                if  let json = data as? Dictionary<String, AnyObject>
                {
                    let weatherData = WLForecastData(json: json)
                    
                    completionHandler(true, nil, weatherData)
                }
                else
                {
                    print("json fail")
                    
                    completionHandler(false, "JSON Parse Failed", nil)
                }
            }
            else
            {
                print("Weather Grab Ended with error: \(error?.localizedDescription ?? "" )")

                completionHandler(false, error?.localizedDescription, nil)
            }
        }
        task.resume()
    }
    
    func grabForecastForCity(_ cityID: Int, _ completionHandler: @escaping ((_ success: Bool, _ errorStr: String?, _ data: WLForecastData?) -> Void))
    {
        let paramsStr   = "id=\(cityID)&appid=\(apiKey)&units=metric"
        let manager     = AFHTTPSessionManager()
        
        guard let requestUrl    = URL(string: baseForecastURL + paramsStr ) else
        {
            completionHandler(false, "requestUrl failed", nil)
            return
        }
        
        let urlRequest  = URLRequest(url: requestUrl )
        
        let task    = manager.dataTask(with: urlRequest  , uploadProgress: nil, downloadProgress: nil)
        { (response, data, error) in
            
            if  error == nil
            {
                print("Weather Grab Successful")
                
                if  let json = data as? Dictionary<String, AnyObject>
                {
                    let weatherData = WLForecastData(json: json)
                    
                    completionHandler(true, nil, weatherData)
                }
                else
                {
                    print("json fail")
                    
                    completionHandler(false, "JSON Parse Failed", nil)
                }
            }
            else
            {
                print("Weather Grab Ended with error: \(error?.localizedDescription ?? "" )")

                completionHandler(false, error?.localizedDescription, nil)
            }
        }
        task.resume()
    }
     
}
